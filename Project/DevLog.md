# Development log

**What to include**  
- [x] Make a Contact page  
- [x] Add phone nr. email and LinkedIn  
- [x] Make a project page  
- [x] Add different gitlab pages with short description  
- [x] Make a about me page  
- [x] Deploy website
- [ ] Include work experience 
- [ ] Add photos 
- [ ] Include different certificates
- [ ] Update visuals
- [ ] Add project report to the gitlab page  
---
**Week 46**
- Made Project description and gitlab

**Week 47**
- Made test page using flask
- Watched this [tutorial](https://www.youtube.com/watch?v=Z1RJmh_OqeA&t=2290s) on youtube about flask
- Made a DevLog markdown
- Watched this [tutorial](https://www.youtube.com/watch?v=4iXyc20Tcao) on how to make one page scroll using html
- Watched some of this [tutorial](https://www.youtube.com/watch?v=1Rs2ND1ryYc) to get a base level understanding of css
- Found and followed this [tutorial](https://pythonhow.com/building-a-website-with-python-flask/) to make a proper website
- Had to delete test site since it messed with the project
- Added projects to project page
- Made changes to the visuals
- Downloaded heroku to deploy the webpage
    - getting an error when checking if it is running
- Updated the home page with some more relevant txt

**Week 48**
- Manged to get the webpage deployed to this [link](https://python-rtfolio.herokuapp.com/)
- Added new folder for pictures
- Tried to add photo to About me page, did not work yet.
- Links to projects not working as intended yet

**Week 49**
- Was bedridden so no progress was made

**Week 50**
- Started work on project report that is due this week
- Made it so that the gitlab repository for this project is public

**Week 52**
- Started to redo the styling of the website
    - started going for a dark colour scheme, but not quite sure about the current look either may need to be reworked as well.
- Still can't make img show for unknown reasons