from flask import Flask, render_template, url_for

app = Flask(__name__)


# to run in virtual environment type when in <<Online_Portfoli>> dir
# virtual\Scripts\python portfolio.py


@app.route('/')
def home():
    return render_template('home.html')


@app.route('/about/')
def about():
    return render_template('about.html')


@app.route('/portfolio/')
def portfolio():
    return render_template('portfolio.html')


@app.route('/contact/')
def contact():
    return render_template('contact.html')


if __name__ == "__main__":
    app.run(debug=True)
