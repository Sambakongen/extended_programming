"""
Exercise 5
Create a chain of command that returns some information about an animal.
The request takes the form of a string like “Dog”.
The information returned is also a string like “Dogs have 4 legs and barks”.
Each command in the chain is responsible for a single type of animal.
"""


class IHandler:
    def setNext(self, handler):
        pass

    def handle(self, request):
        pass


class PetInfo(IHandler):
    nextHandler: IHandler = None

    def setNext(self, handler: IHandler):
        self.nextHandler = handler
        return handler

    def handle(self, request):
        if self.nextHandler:
            return self.nextHandler.handle(request)
        return None


class DogInfo(PetInfo):
    def handle(self, request):
        if request == "Dog":
            return "Dogs have four legs and barks"
        return super().handle(request)


class CatInfo(PetInfo):
    def handle(self, request):
        if request == "Cat":
            return "Cats have four legs and meow"
        return super().handle(request)


class FishInfo(PetInfo):
    def handle(self, request):
        if request == "Fish":
            return "Fish does not have legs and does not bark or meow"
        return super().handle(request)


catInfo = CatInfo()
dogInfo = DogInfo()
fishInfo = FishInfo()

petInfo = PetInfo()

petInfo.setNext(catInfo).setNext(dogInfo).setNext(fishInfo)

print(petInfo.handle("Dog"))
print(petInfo.handle("Cat"))
print(petInfo.handle("Fish"))
