# Exercises for w46  
  
**Exercise 1**  
Create a singleton used for globally accessing some user information like first name, last name and year of birth.  
  
**Exercise 2**  
Create a factory able to produce pet objects with different subclasses based on an input string giving the name of the animal.  
Eg. petFactory.getPet(“Dog”) returns a Dog object where the superclass of the object is Pet petFactory.getPet(“Cat”)  
returns a Cat object where the superclass of the object is Pet  
  
**Exercise 3**  
Create a builder that creates new user object.  
The input when creating a new user should only require a nickname.  
The builder should also support creating new users with their year of birth, email, phone number etc.  
These inputs is given as an array of extra inputs. Eg. userBuilder(“JamesRulez”, [“yob”, 1984, “email”, “James@mail.com”])  
  
**Exercise 4**  
Create an object pool called PetCafe, containing a pool of Pet objects.  
A user should be able to get a pet object if there are any free Pet objects available.  
The user should also be able to give back the Pet object when they are done with it.  
  
**Exercise 5**  
Create a chain of command that returns some information about an animal.  
The request takes the form of a string like “Dog”.  
The information returned is also a string like “Dogs have 4 legs and barks”.  
Each command in the chain is responsible for a single type of animal.  
  
**Exercise 6**  
Make a user object observable so that other object are able to be notified when the name of a user changes.  
  
**Exercise 7**  
Make a simulation of a traffic light using the state pattern.  