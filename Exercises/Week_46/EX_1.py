"""
Exercise 1
Create a singleton used for globally accessing some user information like first name, last name and year of birth.
"""


class UserInfo:

    def __init__(self, FirstName, LastName, YoB):
        self.FirstName = FirstName
        self.LastName = LastName
        self.YoB = YoB


class Singelton:
    __instance = None

    @staticmethod
    def getInstance():
        if Singelton.__instance == None:
            Singelton()
        return Singelton.__instance

    def __init__(self):
        if Singelton.__instance != None:
            raise Exception("This class is a singelton")
        else:
            Singelton.__instance = self

    def getUserInfo(UserInfo):
        print("First name: " + UserInfo.FirstName)
        print("Last name: " + UserInfo.LastName)
        print("Year of Birth: " + str(UserInfo.YoB))


# Test
user = UserInfo("Tom", "Brady", 1990)
s = Singelton.getInstance()
Singelton.getUserInfo(user)
user2 = UserInfo("Jeppe", "Kofoed", 1975)
Singelton.getUserInfo(user2)
