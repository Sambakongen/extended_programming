"""
Exercise 4
Create an object pool called PetCafe, containing a pool of Pet objects.
A user should be able to get a pet object if there are any free Pet objects available.
The user should also be able to give back the Pet object when they are done with it.
"""


class Pets:
    pass


class PetCafe:
    def __init__(self, pets):
        self.objs = [Pets() for _ in range(pets)]

    def aquire(self):
        return self.objs.pop()

    def release(self, obj):
        self.objs.append(obj)

    def freeAmount(self):
        print(len(self.objs))


cafe = PetCafe(2)
cafe.freeAmount()
cafe.aquire()
cafe.freeAmount()
cafe.aquire()
cafe.freeAmount()
cafe.release(1)
cafe.freeAmount()
cafe.release(1)
cafe.freeAmount()
