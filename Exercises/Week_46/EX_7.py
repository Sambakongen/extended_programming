"""
Exercise 7
Make a simulation of a traffic light using the state pattern.
"""


class LightState:
    state = None

    def __init__(self, state):
        self.transitionTo(state)

    def transitionTo(self, state):
        print("Changing state to ", type(state))
        self.state = state
        self.state.context = self

    def changeLight1(self):
        self.state.handle1()

    def changeLight2(self):
        self.state.handle2()


class LightSwitch:
    context = None

    def handle1(self):
        pass

    def handle2(self):
        pass


class GreenLight(LightSwitch):
    def handle1(self):
        print("Changing to green light")

    def handle2(self):
        print("Changing to yellow light")
        self.context.transitionTo(YellowLight())


class YellowLight(LightSwitch):
    def handle1(self):
        print("Changing to yellow light")

    def handle2(self):
        print("Changing to red light")
        self.context.transitionTo(RedLight())


class RedLight(LightSwitch):
    def handle1(self):
        print("Changing to red light")

    def handle2(self):
        print("Changing to green light")
        self.context.transitionTo(GreenLight())


# Test
lightController = LightState(GreenLight())

lightController.changeLight1()
lightController.changeLight2()
lightController.changeLight1()
lightController.changeLight2()
lightController.changeLight1()
lightController.changeLight2()