"""
Exercise 6
Make a user object observable so that other object are able to be notified when the name of a user changes.
"""


class Users:
    def __init__(self, firstName, lastName, yob):
        self.firstName = firstName
        self.lastName = lastName
        self.yob = yob
        self.subscribers = []

    # function to make the User class publisher
    def addSub(self, subscriberFunction):
        self.subscribers.append(subscriberFunction)

    def remSub(self, subscriberFunction):
        self.subscribers.remove(subscriberFunction)

    def changeFirstName(self, newFirstName):
        for i in self.subscribers:  # For loop to publish the event to subscribers
            i()
        self.firstName = newFirstName
        return self.firstName

    def printInfo(self):
        print("Full name: " + self.firstName + " " + self.lastName)
        print("Year of Birth: " + str(self.yob))


class SubscriberA:
    def reactToEvent(self):
        print("First name was changed")


user = Users("Jeppe", "Jensen", 1986)

subscriberA = SubscriberA()
user.addSub(subscriberA.reactToEvent)

user.printInfo()
user.changeFirstName("Karl")
user.printInfo()
