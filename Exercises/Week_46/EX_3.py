"""
Exercise 3
Create a builder that creates new user object.
The input when creating a new user should only require a nickname.
The builder should also support creating new users with
their year of birth, email, phone number etc.
These inputs is given as an array of extra inputs. Eg.
userBuilder(“JamesRulez”, [“yob”, 1984, “email”, “James@mail.com”])
"""


class UserBuilder:
    def __init__(self, nickName, l):
        self.nickName = nickName
        self.yob = 0
        self.firstname = ""
        self.lastname = ""
        self.email = ""
        self.phone = 0
        self.l = l

    def readList(self):
        info = list(self.l)
        for i in info:
            if "firstname" in info:
                x = info.index("firstname")
                self.firstname = info.pop(x+1)
                info.remove("firstname")
            if "lastname" in info:
                x = info.index("lastname")
                self.lastname = info.pop(x+1)
                info.remove("lastname")
            if "yob" in info:
                x = info.index("yob")
                self.yob = info.pop(x +1)
                info.remove("yob")
            if "phone" in info:
                x = info.index("phone")
                self.phone = info.pop(x+1)
                info.remove("phone")
            if "email" in info:
                x = info.index("email")
                self.email = info.pop(x+1)
                info.remove("email")




    def printUserInfo(self):
        if self.l == 0:
            print("nickname: "+self.nickName+"\n")
        else:
            UserBuilder.readList(self)
            print("nickname: "+self.nickName)
            print("full name: "+self.firstname+" "+self.lastname)
            print("Year of Birth: "+str(self.yob))
            print("Phone Number: "+str(self.phone))
            print("Email: "+self.email+"\n")


test = UserBuilder("Palle", 0)
test.printUserInfo()

test1 = UserBuilder("Kalle P",
                    ["phone", 21546542, "firstname", "Kalle", "lastname", "Petersen", "email", "KalleP@mail.com", "yob", 1986])
test1.printUserInfo()
