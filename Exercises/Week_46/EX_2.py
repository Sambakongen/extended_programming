"""
Exercise 2
Create a factory able to produce pet objects with different subclasses based on an input string giving the name of the animal.
Eg. petFactory.getPet(“Dog”) returns a Dog object where the superclass of the object is Pet petFactory.getPet(“Cat”)
returns a Cat object where the superclass of the object is Pet
"""


class Pet:

    def __init__(self, name):
        self.name = name

    def petType(self):
        print(self.name)


class Dog(Pet):

    def __init__(self):
        Pet.__init__(self, "Dog")


class Cat(Pet):

    def __init__(self):
        Pet.__init__(self, "Cat")


class PetFactory():
    @staticmethod
    def getPet(name):
        if name == "Dog":
            return Dog()
        if name == "Cat":
            return Cat()
        return Pet(name)


d = PetFactory.getPet("Dog")
print(type(d))
d.petType()

c = PetFactory.getPet("Cat")
print(type(c))
c.petType()
