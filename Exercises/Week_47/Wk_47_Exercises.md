# Exercises for week 47

**Exercise 1**  
Finish up exercises from week 46.

**Exercise 2**  
Analyse and adjust your solution to the exercises from week 46  
in relation to the Single Responsibility Principle.

**Exercise 3**  
Analyse and adjust your solution to the exercises from week 46  
in relation to the Open-Closed Principle.

**Exercise 4**  
Analyse and adjust your solution to the exercises from week 46  
in relation to the Liskov Substitution Principle.

**Exercise 5**  
Analyse and adjust your solution to the exercises from week 46  
in relation to the Interface Segregation Principle.

**Exercise 6**  
Analyse and adjust your solution to the exercises from week 46  
in relation to the Dependency Inversion Principle.