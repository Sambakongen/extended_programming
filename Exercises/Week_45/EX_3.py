"""
Exercise 3
Create a class for triangles that contains the length of one side of the square,
the corresponding height from the side and a function called getArea() that returns the area of the triangle.
Test the implementation by creating an object with the sides 2 and 4 and use the function to calculate the area of 4.
"""
class triangle:
    def __init__(self,a,b):
        self.a = a
        self.b = b

    def getArea(self):
        area = 1/2*(self.a*self.b)
        print(area)

TiangleObject = triangle(2,4)
TiangleObject.getArea()