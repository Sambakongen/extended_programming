# Exercises for w45

**Exercise 1**  
Create a class for squares that contains the length of the sides of the square and a function called getArea()  
that returns the area of the square. Test the implementation by creating an object  
with the sides 2 and 4 and use the function to calculate the area of 8.  

**Exercise 2**  
Create a class for circles that contains the radius of the circle,  
a function that return the circumference of the circle and a function called getArea()  
that returns the area of the circle. Test the implementation by creating an object with the radius of 2  
and calculate the circumference of 12.6 and the area of around 12.6.  

**Exercise 3**  
Create a class for triangles that contains the length of one side of the square,  
the corresponding height from the side and a function called getArea() that returns the area of the triangle.  
Test the implementation by creating an object with the sides 2 and 4 and use the function to calculate the area of 4.  

**Exercise 4**  
Create a generic shapes class that contains a variable called name set to “shape”, a function called printName() which prints the name.  
Test the implementation by creating an object and calling printName(). The name printed should be “shape”.  

**Exercise 5**  
Rework the square class to be a subclass of the generic shapes class and set the name variable to be “square” on init of a new object.  
Test the implementation like in exercise 1 and 4. The name printed should be “square”.  

**Exercise 6**  
Rework the circle class to be a subclass of the generic shapes class and set the name variable to be “circle” on init of a new object.  
Test the implementation like in exercise 2 and 4. The name printed should be “circle”.  

**Exercise 7**  
Rework the triangle class to be a subclass of the generic shapes class and set the name variable to be “triangle” on init of a new object.  
Test the implementation like in exercise 3 and 4. The name printed should be “triangle”.  

**Exercise 8**  
Extend the shapes class with a function called getArea() which return the value 0. This function should be overriden by the subclasses.  
Test the implementation by creating a shape object and calling the getArea() function which should return 0.  

**Exercise 9**  
Create a function called sumAreas() which takes in 2 shape objects and calculates the total area taken up by the two shapes.  
Test this by first creating a square object, with the sides 2 and 4, and a circle object,  
with the radius of 2. Then use these two object as inputs when running the function. The function should return a value around 20.6.  