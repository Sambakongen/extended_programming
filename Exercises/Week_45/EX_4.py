"""
Exercise 4
Create a generic shapes class that contains a variable called name set to “shape”, a function called printName()
which prints the name.Test the implementation by creating an object and calling printName().
The name printed should be “shape”.
"""


class shape:

    def __init__(self):
        self.name = "shape"

    def printName(self):
        print(self.name)


shapeObject = shape()
shapeObject.printName()
