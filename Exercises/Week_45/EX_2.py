"""
Exercise 2
Create a class for circles that contains the radius of the circle,
a function that return the circumference of the circle and a function called getArea()
that returns the area of the circle. Test the implementation by creating an object with the radius of 2
and calculate the circumference of 12.6 and the area of around 12.6.
"""

# r=radius
class Circle:
    def __init__(self, r):
        self.r = r

    def getAera(self):
        area = self.r*3.1415
        circ = 2*3.1415*self.r
        print(area)
        print(circ)

CircleObject = Circle(2)
CircleObject.getAera()
