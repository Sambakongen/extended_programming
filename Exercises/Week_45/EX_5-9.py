"""
Exercise 5
Rework the square class to be a subclass of the generic shapes class and set the name variable to be “square” on init of a new object.
Test the implementation like in exercise 1 and 4. The name printed should be “square”.

Exercise 6
Rework the circle class to be a subclass of the generic shapes class and set the name variable to be “circle” on init of a new object.
Test the implementation like in exercise 2 and 4. The name printed should be “circle”.

Exercise 7
Rework the triangle class to be a subclass of the generic shapes class and set the name variable to be “triangle” on init of a new object.
 Test the implementation like in exercise 3 and 4. The name printed should be “triangle”.

Exercise 8
Extend the shapes class with a function called getArea() which return the value 0. This function should be overriden by the subclasses.
Test the implementation by creating a shape object and calling the getArea() function which should return 0.

Exercise 9
Create a function called sumAreas() which takes in 2 shape objects and calculates the total area taken up by the two shapes.
Test this by first creating a square object, with the sides 2 and 4, and a circle object,
with the radius of 2. Then use these two object as inputs when running the function. The function should return a value around 20.6.
"""

class Shape:

    def __init__(self):
        self.name = "shape"

    def printName(self):
        print(self.name)

    def getArea(self):
        area = 0
        print(area)
        return area



shapeObject = Shape()
shapeObject.printName()

# Square class EX_5
class Square(Shape):
    def __init__(self, A, B):
        self.name = "square"
        self.A = A
        self.B = B

    def printName(self):
        print(self.name)

    def getArea(self):
        area = (self.A * self.B)
        print(area)
        return area

print("EX_5")
SquareObject = Square(2, 4)
squareArea = SquareObject.getArea()
SquareObject.printName()


# Circle Class EX_6

class Circle(Shape):
    def __init__(self, r):
        self.r = r
        self.name = "Circle"

    def printName(self):
        print(self.name)

    def getArea(self):
        area = (self.r * self.r) * 3.1415
        print(area)
        return area


    def getCirc(self):
        circ = 2 * 3.1415 * self.r
        print(circ)
print("EX_6")
CircleObject = Circle(2)
circleArea = CircleObject.getArea()
CircleObject.getCirc()
CircleObject.printName()


class triangle(Shape):
    def __init__(self, a, b):
        self.a = a
        self.b = b
        self.name = "Traingle"

    def getArea(self):
        area = 1 / 2 * (self.a * self.b)
        print(area)
        return area


    def printName(self):
        print(self.name)
print("EX_7")
TriangleObject = triangle(2, 4)
triangleArea = TriangleObject.getArea()
TriangleObject.printName()

print("EX_8")
shapeObject.getArea()

print("EX 9")
def sumAreas(a,b):
    totalArea = a + b
    print(totalArea)

sumAreas(squareArea, circleArea)