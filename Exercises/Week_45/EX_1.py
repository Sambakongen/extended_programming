"""
Exercise 1
Create a class for squares that contains the length of the sides of the square and a function called getArea()
that returns the area of the square. Test the implementation by creating an object
with the sides 2 and 4 and use the function to calculate the area of 8.
"""
class Square:
    def __init__(self, A, B):
        self.A = A
        self.B = B

    def getArea(self):
        area = (self.A*self.B)
        print(area)

SquareObject = Square(2, 4)
SquareObject.getArea()
